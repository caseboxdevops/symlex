<?php

use App\ExampleController;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;

/**
 * Class AppControllers
 */
class AppControllers implements ControllerProviderInterface
{
    /**
     * @param Application $app
     * @return ControllerCollection
     */
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->get('/', 'MyApp\\Controller\\ExampleController::indexAction');

        return $controllers;
    }
}
