<?php

use Silex\Application;

/**
 * Class AppKernel
 */
class AppKernel extends Application
{
    use Application\TwigTrait;
    use Application\MonologTrait;
}
