<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Silex\Provider\HttpCacheServiceProvider;
use Silex\ServiceProviderInterface;
use MyApp\Service\ExampleService;

/**
 * Class AppServices
 */
class AppServices implements ServiceProviderInterface
{
    /**
     * @var string
     */
    protected $env;

    /**
     * AppServices constructor
     * @param string $env
     */
    public function __construct($env = 'prod')
    {
        $this->env = $env;
    }

    /**
     * @param Application $app
     */
    public function getCustomServices(Application $app)
    {
        $app['my_app.service.example_service'] = $app->share(
            function ($app) {
                return new ExampleService($app);
            }
        );

        // Event dispatcher
        /** @var \Symfony\Component\EventDispatcher\EventDispatcher $dispatcher */
        $dispatcher = $app['dispatcher'];
        $dispatcher->addSubscriber(new \MyApp\EventSubscriber\ExampleEventSubscriber($app));
        $dispatcher->addListener(
            'on.example.event',
            [new \MyApp\EventListener\ExampleEventListener($app), 'onExampleEvent']
        );
    }

    /**
     * @param Application $app
     */
    public function register(Application $app)
    {
        // Directory structure
        $app['app.dir'] = __DIR__;
        $app['app.cache.dir'] = __DIR__.'/cache/'.$this->env;
        $app['app.logs.dir'] = __DIR__.'/logs';
        $app['app.views.dir'] = __DIR__.'/views';
        $app['app.web.dir'] = __DIR__.'/../web';
        $app['app.env'] = $this->env;
        $app['app.charset'] = 'UTF-8';

        $app->error(
            function (\Exception $e, $code) {
                switch ($code) {
                    case 404:
                        $message = 'The requested page could not be found.';
                        break;
                    default:
                        $message = $e->getMessage();
                }

                return new Response($message);
            }
        );

        $app->register(
            new HttpCacheServiceProvider(),
            [
                'http_cache.cache_dir' => $app['app.cache.dir'],
                'http_cache.esi' => null,
            ]
        );

        $app->register(
            new Silex\Provider\MonologServiceProvider(),
            [
                'monolog.logfile' => $app['app.logs.dir'].'/'.$this->env.'.log',
            ]
        );

        $app['twig'] = $app->share(
            function ($app) {
                $loader = new \Twig_Loader_Filesystem($app['app.views.dir']);
                $options = [
                    'cache' => $app['app.cache.dir'],
                    'strict_variables' => false,
                    'debug' => $app['debug'],
                ];

                return new \Twig_Environment($loader, $options);
            }
        );

        $this->getCustomServices($app);
    }

    /**
     * @param Application $app
     */
    public function boot(Application $app)
    {
    }
}
