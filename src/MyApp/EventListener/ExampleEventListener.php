<?php

namespace MyApp\EventListener;

use MyApp\Event\ExampleEvent;
use Silex\Application;

/**
 * Class ExampleEventListener
 */
class ExampleEventListener
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * ExampleEventListener constructor
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function onExampleEvent(ExampleEvent $event)
    {
        // Some actions.
    }
}
