<?php

namespace MyApp\Service;

use MyApp\Event\ExampleEvent;
use Silex\Application;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Class ExampleService
 */
class ExampleService
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * ExampleService constructor
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getIndex()
    {
        $vars = [
            'vars' => ['foo', 'bar', 'baz'],
        ];

        // Dispatch event from service
        /** @var EventDispatcher $dispatcher */
        $dispatcher = $this->app['dispatcher'];
        $event = new ExampleEvent(['foo', 'bar', 'baz']);
        $dispatcher->dispatch('on.example.event', $event);

        $this->app['monolog']->addDebug('Testing the Monolog logging.');

        return $this->app['twig']->render('example.html.twig', $vars);
    }
}
