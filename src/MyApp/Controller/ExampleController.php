<?php

namespace MyApp\Controller;

use MyApp\Event\ExampleEvent;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ExampleController
 */
class ExampleController
{
    /**
     * @param Request $request
     * @param Application $app
     * @return Response
     */
    public function indexAction(Request $request, Application $app)
    {
        // Dispatch event from controller
        // $event = new ExampleEvent(['foo', 'bar', 'baz']);
        // $app['dispatcher']->dispatch('on.example.event', $event);

        $result = $app['my_app.service.example_service']->getIndex();

        $response = new Response($result);
        $response->setStatusCode(Response::HTTP_OK);
        $response->setTtl(60 * 60);

        return $response;
    }
}
