<?php

namespace MyApp\EventSubscriber;

use MyApp\Event\ExampleEvent;
use Silex\Application;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ExampleEventSubscriber
 */
class ExampleEventSubscriber implements EventSubscriberInterface
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * ExampleEventSubcriber constructor
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function onExampleEvent(ExampleEvent $event)
    {
        // Some actions.
    }

    /**
     * @return array
     */
    static function getSubscribedEvents()
    {
        return ['on.example.event' => 'onExampleEvent'];
    }
}
