<?php

namespace MyApp\Event;

use Silex\Application;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ExampleEvent
 */
class ExampleEvent extends Event
{
    /**
     * @var array
     */
    protected $params;

    /**
     * ExampleEvent constructor
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }
}
