<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new AppKernel(['debug' => false]);

$app->register(new AppServices('prod'), []);
$app->mount('/', new AppControllers());

$app['http_cache']->run();
//$app->run();
